//
// Created by Alexandro on 24.01.2021.
//

#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_MEMORY_TEST_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_MEMORY_TEST_H
#include <stdio.h>
#include "mem.h"

void test_one(void *heap);
void test_two(void *heap);
void test_three(void *heap);
void test_four(void *heap);
void test_five(void *heap);
#endif //ASSIGNMENT_MEMORY_ALLOCATOR_MEMORY_TEST_H
